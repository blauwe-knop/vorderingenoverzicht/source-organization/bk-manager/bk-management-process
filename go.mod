module gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process

go 1.22.3

require (
	github.com/go-chi/chi/v5 v5.2.0
	github.com/go-chi/cors v1.2.1
	github.com/jessevdk/go-flags v1.6.1
	github.com/prometheus/client_golang v1.20.5
	github.com/stretchr/testify v1.10.0
	gitlab.com/blauwe-knop/common/health-checker v0.0.8
	gitlab.com/blauwe-knop/connect/go-connect v1.0.9
	gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service v0.18.1
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process v0.17.18
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service v0.17.15
	go.uber.org/mock v0.5.0
	go.uber.org/zap v1.27.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.62.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	google.golang.org/protobuf v1.36.3 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
