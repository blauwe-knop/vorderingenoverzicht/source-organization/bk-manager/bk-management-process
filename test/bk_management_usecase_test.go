// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package bkmanagementprocess_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"

	servicemodel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"

	bkConfigModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"

	schemeProcessRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/test/helpers"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/test/mock"
)

func TestBkManagementUseCase(t *testing.T) {
	mockOrganization := &model.Organization{
		Oin:          "Oin",
		Name:         "Organization",
		DiscoveryUrl: "DiscoveryAddress",
	}

	mockSchemeModelOrganization := &servicemodel.Organization{
		Oin:          mockOrganization.Oin,
		Name:         mockOrganization.Name,
		DiscoveryUrl: mockOrganization.DiscoveryUrl,
	}

	t.Run("Register organization", func(t *testing.T) {
		observedZapCore, _ := observer.New(zap.InfoLevel)
		observedLogger := zap.New(observedZapCore)

		cryptographyTestHelper := helpers.NewCryptographyTestHelper(
			observedLogger,
		)

		keyPair, err := cryptographyTestHelper.GenerateKeyPair()
		if err != nil {
			t.Errorf("generate app key pair failed: %v", err)
		}

		type fields struct {
			schemeRepository   schemeProcessRepositories.SchemeRepository
			bkConfigRepository bkConfigRepositories.BkConfigRepository
		}
		type args struct {
		}

		tests := []struct {
			name          string
			fields        fields
			args          args
			expectedError error
		}{
			{
				name: "create organization when organization does not exist",
				fields: fields{
					schemeRepository: func() schemeProcessRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().FetchOrganization(mockOrganization.Oin).Return(nil, schemeProcessRepositories.ErrOrganizationNotFound).AnyTimes()
						repo.EXPECT().RegisterOrganization(gomock.Any()).Return(mockSchemeModelOrganization, nil).AnyTimes()
						return repo
					}(),
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{{Id: "id", PublicKey: keyPair.PublicKey, PrivateKey: keyPair.PrivateKey}}, nil).AnyTimes()
						return repo
					}(),
				},
				args:          args{},
				expectedError: nil,
			}, {
				name: "error retrieving organization",
				fields: fields{
					schemeRepository: func() schemeProcessRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().FetchOrganization(mockOrganization.Oin).Return(nil, fmt.Errorf("some error")).AnyTimes()
						repo.EXPECT().RegisterOrganization(gomock.Any()).Times(0)
						return repo
					}(),
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{{Id: "id", PublicKey: keyPair.PublicKey, PrivateKey: keyPair.PrivateKey}}, nil).AnyTimes()
						return repo
					}(),
				},
				args:          args{},
				expectedError: usecases.ErrRetrievingOrganization,
			}, {
				name: "error creating organization",
				fields: fields{
					schemeRepository: func() schemeProcessRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().FetchOrganization(mockOrganization.Oin).Return(nil, schemeProcessRepositories.ErrOrganizationNotFound).AnyTimes()
						repo.EXPECT().RegisterOrganization(gomock.Any()).Return(nil, fmt.Errorf("some error")).AnyTimes()
						return repo
					}(),
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{{Id: "id", PublicKey: keyPair.PublicKey, PrivateKey: keyPair.PrivateKey}}, nil).AnyTimes()
						return repo
					}(),
				},
				args:          args{},
				expectedError: usecases.ErrCreatingOrganization,
			}, {
				name: "organization already exists",
				fields: fields{
					schemeRepository: func() schemeProcessRepositories.SchemeRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockSchemeRepository(ctrl)
						repo.EXPECT().FetchOrganization(mockOrganization.Oin).Return(mockSchemeModelOrganization, nil).AnyTimes()
						repo.EXPECT().RegisterOrganization(gomock.Any()).Times(0)
						return repo
					}(),
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{{Id: "id", PublicKey: keyPair.PublicKey, PrivateKey: keyPair.PrivateKey}}, nil).AnyTimes()
						return repo
					}(),
				},
				args:          args{},
				expectedError: nil,
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				observedZapCore, _ := observer.New(zap.InfoLevel)
				observedLogger := zap.New(observedZapCore)
				bkManagementUseCase := usecases.NewBkManagementUseCase(observedLogger, tt.fields.schemeRepository, tt.fields.bkConfigRepository, *mockOrganization)
				err := bkManagementUseCase.Register()
				assert.Equal(t, tt.expectedError, err)
			})
		}
	})

	t.Run("Generate key pair", func(t *testing.T) {
		type fields struct {
			schemeRepository   schemeProcessRepositories.SchemeRepository
			bkConfigRepository bkConfigRepositories.BkConfigRepository
		}
		type args struct {
		}

		tests := []struct {
			name          string
			fields        fields
			args          args
			expectedError error
		}{
			{
				name: "create keypair when keypair does not exist",
				fields: fields{
					schemeRepository: nil,
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{}, nil).AnyTimes()
						repo.EXPECT().CreateKeyPair(gomock.Any()).Return(&bkConfigModel.KeyPair{Id: "id", PublicKey: "public-key", PrivateKey: "public-key"}, nil).AnyTimes()
						return repo
					}(),
				},
				args:          args{},
				expectedError: nil,
			}, {
				name: "do not generate a new keypair when a keypair already exists",
				fields: fields{
					schemeRepository: nil,
					bkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
						ctrl := gomock.NewController(t)
						defer ctrl.Finish()

						repo := mock.NewMockBkConfigRepository(ctrl)
						repo.EXPECT().ListKeyPairs().Return(&[]bkConfigModel.KeyPair{{Id: "id", PublicKey: "public-key", PrivateKey: "public-key"}}, nil).AnyTimes()
						repo.EXPECT().CreateKeyPair(gomock.Any()).Times(0)
						return repo
					}(),
				},
				args:          args{},
				expectedError: usecases.ErrKeyPairExists,
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				observedZapCore, _ := observer.New(zap.InfoLevel)
				observedLogger := zap.New(observedZapCore)
				bkManagementUseCase := usecases.NewBkManagementUseCase(observedLogger, tt.fields.schemeRepository, tt.fields.bkConfigRepository, *mockOrganization)
				err := bkManagementUseCase.GenerateKeyPair()
				assert.Equal(t, tt.expectedError, err)
			})
		}
	})

}
