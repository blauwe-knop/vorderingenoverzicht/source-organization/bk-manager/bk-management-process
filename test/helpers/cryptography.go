// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"crypto/elliptic"
	"fmt"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"go.uber.org/zap"
)

type CryptographyTestHelper struct {
	Logger *zap.Logger
}

func NewCryptographyTestHelper(logger *zap.Logger) *CryptographyTestHelper {
	return &CryptographyTestHelper{
		Logger: logger,
	}
}

func (uc *CryptographyTestHelper) GenerateKeyPair() (*KeyPair, error) {
	uc.Logger.Debug("generate key pair")
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		uc.Logger.Error("generate key pair failed", zap.Error(err))
		return nil, fmt.Errorf("generate key pair failed: %v", err)
	}

	uc.Logger.Info("generate key pair succeeded", zap.Reflect("privateKey", privateKey))

	privateKeyPem, err := privateKey.ToPem()
	if err != nil {
		uc.Logger.Error("EncodePrivateKeyToPem failed", zap.Error(err))
		return nil, fmt.Errorf("EncodePrivateKeyToPem failed: %v", err)
	}

	publicKeyPem, err := privateKey.PublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("EncodePublicKeyToPem failed", zap.Error(err))
		return nil, fmt.Errorf("EncodePublicKeyToPem failed: %v", err)
	}

	return &KeyPair{
		PublicKey:  string(publicKeyPem),
		PrivateKey: string(privateKeyPem),
	}, nil
}
