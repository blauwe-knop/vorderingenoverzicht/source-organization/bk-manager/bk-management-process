// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"crypto/elliptic"
	"errors"
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	schemeProcessRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
	"go.uber.org/zap"

	bkConfigModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/model"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"

	schemeprocessmodel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type BkManagementUseCase struct {
	Logger             *zap.Logger
	SchemeRepository   schemeProcessRepositories.SchemeRepository
	BkConfigRepository bkConfigRepositories.BkConfigRepository
	Organization       model.Organization
}

func NewBkManagementUseCase(logger *zap.Logger, schemeRepository schemeProcessRepositories.SchemeRepository, bkConfigRepository bkConfigRepositories.BkConfigRepository, organization model.Organization) *BkManagementUseCase {
	return &BkManagementUseCase{
		Logger:             logger,
		SchemeRepository:   schemeRepository,
		BkConfigRepository: bkConfigRepository,
		Organization:       organization,
	}
}

var ErrRetrievingOrganization = errors.New("error retrieving organization")
var ErrCreatingOrganization = errors.New("error creating organization")
var ErrKeyPairExists = errors.New("error key pair exists")
var ErrKeyPairDoesNotExist = errors.New("error key pair does not exist")

func (uc *BkManagementUseCase) FetchOrganizationSettings() model.Organization {
	return uc.Organization
}

func (uc *BkManagementUseCase) FetchRegistrationState() (*model.SchemeState, error) {
	organization, err := uc.SchemeRepository.FetchOrganization(uc.Organization.Oin)
	if errors.Is(err, schemeProcessRepositories.ErrOrganizationNotFound) {
		event := events.MP_21
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return &model.SchemeState{
				Registered: false,
				Approved:   false},
			nil
	}
	if err != nil {
		event := events.MP_22
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to get organization: %v", err)
	}

	return &model.SchemeState{
			Registered: true,
			Approved:   organization.Approved},
		nil
}

func (uc *BkManagementUseCase) Register() error {
	_, err := uc.SchemeRepository.FetchOrganization(uc.Organization.Oin)

	if errors.Is(err, schemeProcessRepositories.ErrOrganizationNotFound) {
		event := events.MP_21
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		keyPairList, secondaryErr := uc.BkConfigRepository.ListKeyPairs()
		if secondaryErr != nil {
			event := events.MP_23
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return fmt.Errorf("failed to list key pairs: %v", secondaryErr)
		}
		if len(*keyPairList) == 0 {
			event := events.MP_5
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return ErrKeyPairDoesNotExist
		}

		// current implementation: we always pick the first one if multiple keys exist
		keyPair := (*keyPairList)[0]

		_, err = ec.ParsePublicKeyFromPem([]byte(keyPair.PublicKey))
		if err != nil {
			event := events.MP_24
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return fmt.Errorf("failed to decode public key from pem: %v", err)
		}

		organization := schemeprocessmodel.Organization{
			Oin:          uc.Organization.Oin,
			Name:         uc.Organization.Name,
			DiscoveryUrl: uc.Organization.DiscoveryUrl,
			PublicKey:    keyPair.PublicKey,
			LogoUrl:      uc.Organization.LogoUrl,
		}

		_, err = uc.SchemeRepository.RegisterOrganization(organization)
		if err != nil {
			event := events.MP_10
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return ErrCreatingOrganization
		}
	} else if err != nil {
		event := events.MP_22
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return ErrRetrievingOrganization
	}

	return nil
}

func (uc *BkManagementUseCase) FetchPublicKey() (string, error) {
	keyPairList, err := uc.BkConfigRepository.ListKeyPairs()
	if err != nil {
		event := events.MP_23
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to get key pair list: %v", err)
	}
	if len(*keyPairList) == 0 {
		event := events.MP_5
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", ErrKeyPairDoesNotExist
	}

	// current implementation: we always pick the first one if multiple keys exist
	keyPair := (*keyPairList)[0]

	return keyPair.PublicKey, nil
}

func (uc *BkManagementUseCase) GenerateKeyPair() error {
	keyPairList, err := uc.BkConfigRepository.ListKeyPairs()
	if err != nil {
		event := events.MP_23
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to retrieving key pair list: %v", err)
	}
	// We only create a key if no keys exists
	if len(*keyPairList) > 0 {
		event := events.MP_6
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return ErrKeyPairExists
	}

	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		event := events.MP_7
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to GenerateKey: %v", err)
	}

	privateKeyPem, err := privateKey.ToPem()
	if err != nil {
		event := events.MP_25
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to encode private key pem: %v", err)
	}

	publicKeyPem, err := privateKey.PublicKey.ToPem()
	if err != nil {
		event := events.MP_26
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to encode public key pem: %v", err)
	}

	bkKeyPair := bkConfigModel.KeyPair{PrivateKey: string(privateKeyPem), PublicKey: string(publicKeyPem)}
	_, err = uc.BkConfigRepository.CreateKeyPair(bkKeyPair)
	if err != nil {
		event := events.MP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return fmt.Errorf("failed to create key pair: %v", err)
	}
	return nil
}

func (uc *BkManagementUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.BkConfigRepository,
	}
}
