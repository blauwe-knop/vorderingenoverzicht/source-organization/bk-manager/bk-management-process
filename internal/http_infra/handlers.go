// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/events"
)

func handlerFetchOrganizationSettings(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	bkManagementUseCase, _ := context.Value(bkManagementUseCaseKey).(*usecases.BkManagementUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.MP_11
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organizationSettings := bkManagementUseCase.FetchOrganizationSettings()

	responseWriter.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(responseWriter).Encode(organizationSettings)
	if err != nil {
		event := events.MP_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.MP_12
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerFetchPublicKey(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	bkManagementUseCase, _ := context.Value(bkManagementUseCaseKey).(*usecases.BkManagementUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.MP_13
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	publicKey, err := bkManagementUseCase.FetchPublicKey()
	if errors.Is(err, usecases.ErrKeyPairDoesNotExist) {
		event := events.MP_5
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, err.Error(), http.StatusNotFound)
		return
	}
	if err != nil {
		event := events.MP_8
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(publicKey)
	if err != nil {
		event := events.MP_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.MP_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGenerateKeyPair(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	bkManagementUseCase, _ := context.Value(bkManagementUseCaseKey).(*usecases.BkManagementUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.MP_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	err := bkManagementUseCase.GenerateKeyPair()
	if errors.Is(err, usecases.ErrKeyPairExists) {
		event := events.MP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	if err != nil {
		event := events.MP_7
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.MP_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerFetchRegistrationState(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	bkManagementUseCase, _ := context.Value(bkManagementUseCaseKey).(*usecases.BkManagementUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.MP_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	schemeState, err := bkManagementUseCase.FetchRegistrationState()
	if err != nil {
		event := events.MP_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(schemeState)
	if err != nil {
		event := events.MP_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.MP_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerRegister(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	bkManagementUseCase, _ := context.Value(bkManagementUseCaseKey).(*usecases.BkManagementUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.MP_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	err := bkManagementUseCase.Register()
	if errors.Is(err, usecases.ErrKeyPairDoesNotExist) {
		event := events.MP_5
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	if err != nil {
		event := events.MP_10
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.MP_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
