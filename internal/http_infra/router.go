// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/http_infra/metrics"

	"github.com/go-chi/cors"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/usecases"
)

type key int

const (
	bkManagementUseCaseKey key = iota
	loggerKey              key = iota
)

func NewRouter(bkManagementUseCase *usecases.BkManagementUseCase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	}))

	metricsMiddleware := metrics.NewMiddleware("app-management-process")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/fetch_organization_settings", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), bkManagementUseCaseKey, bkManagementUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchOrganizationSettings(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_public_key", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), bkManagementUseCaseKey, bkManagementUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchPublicKey(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/generate_key_pair", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), bkManagementUseCaseKey, bkManagementUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerGenerateKeyPair(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/fetch_registration_state", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), bkManagementUseCaseKey, bkManagementUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerFetchRegistrationState(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/register", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), bkManagementUseCaseKey, bkManagementUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerRegister(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("bk-management-process", bkManagementUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
