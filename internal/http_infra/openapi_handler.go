// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"net/http"
	"os"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/events"
)

func handlerJson(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	fileBytes, err := os.ReadFile("/api/openapi.json")
	if err != nil {
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/json")
	_, err = responseWriter.Write(fileBytes)

	if err != nil {
		event := events.MP_1
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerYaml(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	fileBytes, err := os.ReadFile("/api/openapi.yaml")
	if err != nil {
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/octet-stream")
	_, err = responseWriter.Write(fileBytes)

	if err != nil {
		event := events.MP_1
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}
