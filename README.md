# bk-management-process

Procescomponent of participating organizations in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the bk management process:

```sh
go run cmd/bk-management-process/*.go --listen-address 0.0.0.0:8080
```

By default, the application will run on port `80`, so specifically set port 8080 to run it locally.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

### Regenerating mocks

```sh
sh scripts/regenerate-gomock-files.sh
```

## Deployment
