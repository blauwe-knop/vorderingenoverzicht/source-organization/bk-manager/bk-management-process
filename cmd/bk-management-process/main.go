// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/bk-management-process/pkg/model"
)

type options struct {
	ListenAddress            string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the bk-management-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SchemeDiscoveryUrl       string `long:"scheme-discovery-url" env:"SCHEME_DISCOVERY_URL" default:"http://localhost:80" description:"Scheme discovery url."`
	BkConfigServiceAddress   string `long:"bk-config-service-address" env:"BK_CONFIG_SERVICE_ADDRESS" default:"http://localhost:80" description:"Bk Config service address."`
	BkConfigServiceAPIKey    string `long:"bk-config-service-api-key" env:"BK_CONFIG_SERVICE_API_KEY" default:"" description:"API key to use when calling the bk-config service."`
	OrganizationName         string `long:"organization-name" env:"ORGANIZATION_NAME" description:"Organization name"`
	OrganizationOin          string `long:"organization-oin" env:"ORGANIZATION_OIN" description:"Organization oin"`
	OrganizationDiscoveryUrl string `long:"organization-discovery-url" env:"ORGANIZATION_DISCOVERY_URL" description:"Organization discovery url"`
	OrganizationLogoUrl      string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo url"`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	schemeRepository := repositories.NewSchemeClient(cliOptions.SchemeDiscoveryUrl)

	bkConfigRepository := bkConfigRepositories.NewBkConfigClient(cliOptions.BkConfigServiceAddress, cliOptions.BkConfigServiceAPIKey)

	bkManagementUseCase := usecases.NewBkManagementUseCase(
		logger,
		schemeRepository,
		bkConfigRepository,
		model.Organization{
			Oin:          cliOptions.OrganizationOin,
			Name:         cliOptions.OrganizationName,
			DiscoveryUrl: cliOptions.OrganizationDiscoveryUrl,
			LogoUrl:      cliOptions.OrganizationLogoUrl,
		},
	)

	router := http_infra.NewRouter(bkManagementUseCase, logger)

	event := events.MP_2
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.MP_3
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	zapLogger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return zapLogger, nil
}
