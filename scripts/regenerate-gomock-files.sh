#!/bin/bash

mockgen -destination=./../test/mock/scheme_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories SchemeRepository
mockgen -destination=./../test/mock/bk_config_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories BkConfigRepository
