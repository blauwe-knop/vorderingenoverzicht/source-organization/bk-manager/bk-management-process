package model

type Organization struct {
	Oin          string `json:"oin"`
	Name         string `json:"name"`
	DiscoveryUrl string `json:"discoveryUrl"`
	LogoUrl      string `json:"logoUrl"`
}
