package model

type SchemeState struct {
	Registered bool `json:"registered"`
	Approved   bool `json:"approved"`
}
