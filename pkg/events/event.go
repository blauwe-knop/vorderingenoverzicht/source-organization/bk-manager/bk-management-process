package events

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type SeverityLevel string

type Event struct {
	ClassId       string        `json:"classId"`
	Message       string        `json:"message"`
	Severity      SeverityLevel `json:"severity"`
	CefVersion    string        `json:"cefVersion"`
	DeviceVendor  string        `json:"deviceVendor"`
	DeviceProduct string        `json:"deviceProduct"`
	DeviceVersion string        `json:"deviceVersion"`
}

const (
	VeryHigh SeverityLevel = "Very-High"
	High     SeverityLevel = "High"
	Medium   SeverityLevel = "Medium"
	Low      SeverityLevel = "Low"
	Unknown  SeverityLevel = "Unknown"
)

const (
	cefVersion    string = "CEF:1"
	deviceVendor  string = "VORIJK"
	deviceProduct string = "bk-management-process"
	deviceVersion string = "1"
)

func NewEvent(classId string, name string, severity SeverityLevel) *Event {
	return &Event{
		ClassId:       classId,
		Message:       name,
		Severity:      severity,
		CefVersion:    cefVersion,
		DeviceVendor:  deviceVendor,
		DeviceProduct: deviceProduct,
		DeviceVersion: deviceVersion,
	}
}

func (e *Event) GetLogLevel() zapcore.Level {
	switch e.Severity {
	case VeryHigh:
		return zap.FatalLevel
	case High:
		return zap.ErrorLevel
	case Medium:
		return zap.WarnLevel
	case Low:
		return zap.InfoLevel
	case Unknown:
		return zap.InfoLevel
	default:
		return zap.InfoLevel
	}
}
