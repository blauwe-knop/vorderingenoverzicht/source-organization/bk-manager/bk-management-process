package events

var (
	MP_1 = NewEvent(
		"mp_1",
		"failed to write file bytes",
		High,
	)
	MP_2 = NewEvent(
		"mp_2",
		"started listening",
		Low,
	)
	MP_3 = NewEvent(
		"mp_3",
		"server closed",
		High,
	)
	MP_4 = NewEvent(
		"mp_4",
		"failed to encode response payload",
		High,
	)
	MP_5 = NewEvent(
		"mp_5",
		"key pair does not exist",
		High,
	)
	MP_6 = NewEvent(
		"mp_6",
		"key pair already exists",
		High,
	)
	MP_7 = NewEvent(
		"mp_7",
		"failed to generate key pair",
		High,
	)
	MP_8 = NewEvent(
		"mp_8",
		"failed to get public key",
		High,
	)
	MP_9 = NewEvent(
		"mp_9",
		"failed to get scheme state",
		High,
	)
	MP_10 = NewEvent(
		"mp_10",
		"failed to register organization",
		High,
	)
	MP_11 = NewEvent(
		"mp_11",
		"received request for organization settings",
		Low,
	)
	MP_12 = NewEvent(
		"mp_12",
		"sent response with organization settings",
		Low,
	)
	MP_13 = NewEvent(
		"mp_13",
		"received request for organization public key",
		Low,
	)
	MP_14 = NewEvent(
		"mp_14",
		"sent response with organization public key",
		Low,
	)
	MP_15 = NewEvent(
		"mp_15",
		"received request to generate organization key pair",
		Low,
	)
	MP_16 = NewEvent(
		"mp_16",
		"organization key pair generated",
		Low,
	)
	MP_17 = NewEvent(
		"mp_17",
		"received request for scheme state",
		Low,
	)
	MP_18 = NewEvent(
		"mp_18",
		"sent response with scheme state",
		Low,
	)
	MP_19 = NewEvent(
		"mp_19",
		"received request to register organization",
		Low,
	)
	MP_20 = NewEvent(
		"mp_20",
		"organization registered",
		Low,
	)
	MP_21 = NewEvent(
		"mp_21",
		"organization not found",
		High,
	)
	MP_22 = NewEvent(
		"mp_22",
		"failed to fetch organization",
		High,
	)
	MP_23 = NewEvent(
		"mp_23",
		"failed to get key pair list",
		High,
	)
	MP_24 = NewEvent(
		"mp_24",
		"failed to decode public key from pem",
		High,
	)
	MP_25 = NewEvent(
		"mp_25",
		"failed to encode private key to pem",
		High,
	)
	MP_26 = NewEvent(
		"mp_26",
		"failed to encode public key to pem",
		High,
	)
	MP_27 = NewEvent(
		"mp_27",
		"failed to store key pair",
		High,
	)
)
