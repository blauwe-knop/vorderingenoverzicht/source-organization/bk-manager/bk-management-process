FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/bk-management-process/api
COPY ./cmd /go/src/bk-management-process/cmd
COPY ./internal /go/src/bk-management-process/internal
COPY ./pkg /go/src/bk-management-process/pkg
COPY ./go.mod /go/src/bk-management-process/
COPY ./go.sum /go/src/bk-management-process/
WORKDIR /go/src/bk-management-process
RUN go mod download \
 && go build -o dist/bin/bk-management-process ./cmd/bk-management-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/bk-management-process/dist/bin/bk-management-process /usr/local/bin/bk-management-process
COPY --from=build /go/src/bk-management-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/bk-management-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/bk-management-process"]
